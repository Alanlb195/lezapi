﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Nivel
    {
        public Nivel()
        {
            Grado = new HashSet<Grado>();
        }

        public sbyte NivelId { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Grado> Grado { get; set; }
    }
}
