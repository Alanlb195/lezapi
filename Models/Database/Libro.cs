﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Libro
    {
        public Libro()
        {
            Bloque = new HashSet<Bloque>();
        }

        public int LibroId { get; set; }
        public string Titulo { get; set; }
        public short InicioNumPaginas { get; set; }
        public short NumPaginas { get; set; }
        public string ImgLibro { get; set; }
        public sbyte GradoId { get; set; }

        public virtual Grado Grado { get; set; }
        public virtual ICollection<Bloque> Bloque { get; set; }
    }
}
