﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class OpcionExamen
    {
        public int OpcionExamenId { get; set; }
        public string Valor { get; set; }
        public int PreguntaExamenId { get; set; }

        public virtual PreguntaExamen PreguntaExamen { get; set; }
    }
}
