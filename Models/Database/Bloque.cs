﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Bloque
    {
        public Bloque()
        {
            Pagina = new HashSet<Pagina>();
        }

        public int BloqueId { get; set; }
        public string Nombre { get; set; }
        public int LibroId { get; set; }

        public virtual Libro Libro { get; set; }
        public virtual Examen Examen { get; set; }
        public virtual ICollection<Pagina> Pagina { get; set; }
    }
}
