﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LezApi.Database
{
    public partial class LearnezContext : DbContext
    {
        public LearnezContext()
        {
        }

        public LearnezContext(DbContextOptions<LearnezContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bloque> Bloque { get; set; }
        public virtual DbSet<Examen> Examen { get; set; }
        public virtual DbSet<Grado> Grado { get; set; }
        public virtual DbSet<Libro> Libro { get; set; }
        public virtual DbSet<Nivel> Nivel { get; set; }
        public virtual DbSet<OpcionExamen> Opcionexamen { get; set; }
        public virtual DbSet<Pagina> Pagina { get; set; }
        public virtual DbSet<PreguntaExamen> Preguntaexamen { get; set; }
        public virtual DbSet<Respuesta> Respuesta { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;user=root;database=Learnez", x => x.ServerVersion("10.4.13-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bloque>(entity =>
            {
                entity.ToTable("bloque");

                entity.HasIndex(e => e.LibroId)
                    .HasName("FK_Bloque-Libro");

                entity.Property(e => e.BloqueId)
                    .HasColumnName("bloqueId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LibroId)
                    .HasColumnName("libroId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Libro)
                    .WithMany(p => p.Bloque)
                    .HasForeignKey(d => d.LibroId)
                    .HasConstraintName("FK_Bloque-Libro");
            });

            modelBuilder.Entity<Examen>(entity =>
            {
                entity.ToTable("examen");

                entity.HasIndex(e => e.BloqueId)
                    .HasName("bloqueId")
                    .IsUnique();

                entity.Property(e => e.ExamenId)
                    .HasColumnName("examenId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BloqueId)
                    .HasColumnName("bloqueId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ciclo)
                    .IsRequired()
                    .HasColumnName("ciclo")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_spanish_ci");

                entity.Property(e => e.NumPreguntas)
                    .HasColumnName("numPreguntas")
                    .HasColumnType("smallint(6)");

                entity.HasOne(d => d.Bloque)
                    .WithOne(p => p.Examen)
                    .HasForeignKey<Examen>(d => d.BloqueId)
                    .HasConstraintName("FK_Examen-Bloque");
            });

            modelBuilder.Entity<Grado>(entity =>
            {
                entity.ToTable("grado");

                entity.HasIndex(e => e.NivelId)
                    .HasName("FK_Grado-Nivel");

                entity.Property(e => e.GradoId)
                    .HasColumnName("gradoId")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.Color)
                    .IsRequired()
                    .HasColumnName("color")
                    .HasColumnType("char(7)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_spanish_ci");

                entity.Property(e => e.Identificador)
                    .HasColumnName("identificador")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.NivelId)
                    .HasColumnName("nivelId")
                    .HasColumnType("tinyint(4)");

                entity.HasOne(d => d.Nivel)
                    .WithMany(p => p.Grado)
                    .HasForeignKey(d => d.NivelId)
                    .HasConstraintName("FK_Grado-Nivel");
            });

            modelBuilder.Entity<Libro>(entity =>
            {
                entity.ToTable("libro");

                entity.HasIndex(e => e.GradoId)
                    .HasName("FK_Libro-Grado");

                entity.Property(e => e.LibroId)
                    .HasColumnName("libroId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GradoId)
                    .HasColumnName("gradoId")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.ImgLibro)
                    .IsRequired()
                    .HasColumnName("imgLibro")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.InicioNumPaginas)
                    .HasColumnName("inicioNumPaginas")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.NumPaginas)
                    .HasColumnName("numPaginas")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("titulo")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Grado)
                    .WithMany(p => p.Libro)
                    .HasForeignKey(d => d.GradoId)
                    .HasConstraintName("FK_Libro-Grado");
            });

            modelBuilder.Entity<Nivel>(entity =>
            {
                entity.ToTable("nivel");

                entity.Property(e => e.NivelId)
                    .HasColumnName("nivelId")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<OpcionExamen>(entity =>
            {
                entity.ToTable("opcionexamen");

                entity.HasIndex(e => e.PreguntaExamenId)
                    .HasName("FK_Opcion-Pregunta");

                entity.Property(e => e.OpcionExamenId)
                    .HasColumnName("opcionExamenId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PreguntaExamenId)
                    .HasColumnName("preguntaExamenId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasColumnName("valor")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_spanish_ci");

                entity.HasOne(d => d.PreguntaExamen)
                    .WithMany(p => p.Opcionexamen)
                    .HasForeignKey(d => d.PreguntaExamenId)
                    .HasConstraintName("FK_Opcion-Pregunta");
            });

            modelBuilder.Entity<Pagina>(entity =>
            {
                entity.ToTable("pagina");

                entity.HasIndex(e => e.BloqueId)
                    .HasName("FK_Pagina-Bloque");

                entity.Property(e => e.PaginaId)
                    .HasColumnName("paginaId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BloqueId)
                    .HasColumnName("bloqueId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ImgPagina)
                    .IsRequired()
                    .HasColumnName("imgPagina")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_spanish_ci");

                entity.Property(e => e.NumPagina)
                    .HasColumnName("numPagina")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("titulo")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Bloque)
                    .WithMany(p => p.Pagina)
                    .HasForeignKey(d => d.BloqueId)
                    .HasConstraintName("FK_Pagina-Bloque");
            });

            modelBuilder.Entity<PreguntaExamen>(entity =>
            {
                entity.ToTable("preguntaexamen");

                entity.HasIndex(e => e.ExamenId)
                    .HasName("FK_PreguntaEx-Examen");

                entity.Property(e => e.PreguntaExamenId)
                    .HasColumnName("preguntaExamenId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ExamenId)
                    .HasColumnName("examenId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NumPregunta)
                    .HasColumnName("numPregunta")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.Pregunta)
                    .IsRequired()
                    .HasColumnName("pregunta")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_spanish_ci");

                entity.Property(e => e.RespuestaId)
                    .HasColumnName("respuestaId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Examen)
                    .WithMany(p => p.Preguntaexamen)
                    .HasForeignKey(d => d.ExamenId)
                    .HasConstraintName("FK_PreguntaEx-Examen");
            });

            modelBuilder.Entity<Respuesta>(entity =>
            {
                entity.HasKey(e => e.PreguntaId)
                    .HasName("PRIMARY");

                entity.ToTable("respuesta");

                entity.HasIndex(e => e.PaginaId)
                    .HasName("FK_Respuesta-Pagina");

                entity.Property(e => e.PreguntaId)
                    .HasColumnName("preguntaId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PaginaId)
                    .HasColumnName("paginaId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Pregunta)
                    .IsRequired()
                    .HasColumnName("pregunta")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Solucion)
                    .IsRequired()
                    .HasColumnName("solucion")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Pagina)
                    .WithMany(p => p.Respuesta)
                    .HasForeignKey(d => d.PaginaId)
                    .HasConstraintName("FK_Respuesta-Pagina");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
