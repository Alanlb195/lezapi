﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Grado
    {
        public Grado()
        {
            Libro = new HashSet<Libro>();
        }

        public sbyte GradoId { get; set; }
        public sbyte Identificador { get; set; }
        public string Color { get; set; }
        public sbyte NivelId { get; set; }

        public virtual Nivel Nivel { get; set; }
        public virtual ICollection<Libro> Libro { get; set; }
    }
}
