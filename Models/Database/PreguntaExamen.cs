﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class PreguntaExamen
    {
        public PreguntaExamen()
        {
            Opcionexamen = new HashSet<OpcionExamen>();
        }

        public int PreguntaExamenId { get; set; }
        public string Pregunta { get; set; }
        public int RespuestaId { get; set; }
        public short NumPregunta { get; set; }
        public int ExamenId { get; set; }

        public virtual Examen Examen { get; set; }
        public virtual ICollection<OpcionExamen> Opcionexamen { get; set; }
    }
}
