﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Respuesta
    {
        public int PreguntaId { get; set; }
        public string Pregunta { get; set; }
        public string Solucion { get; set; }
        public int PaginaId { get; set; }

        public virtual Pagina Pagina { get; set; }
    }
}
