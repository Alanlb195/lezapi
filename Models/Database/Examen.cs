﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Examen
    {
        public Examen()
        {
            Preguntaexamen = new HashSet<PreguntaExamen>();
        }

        public int ExamenId { get; set; }
        public string Ciclo { get; set; }
        public short NumPreguntas { get; set; }
        public int BloqueId { get; set; }

        public virtual Bloque Bloque { get; set; }
        public virtual ICollection<PreguntaExamen> Preguntaexamen { get; set; }
    }
}
