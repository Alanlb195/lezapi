﻿using System;
using System.Collections.Generic;

namespace LezApi.Database
{
    public partial class Pagina
    {
        public Pagina()
        {
            Respuesta = new HashSet<Respuesta>();
        }

        public int PaginaId { get; set; }
        public string Titulo { get; set; }
        public short NumPagina { get; set; }
        public string ImgPagina { get; set; }
        public int BloqueId { get; set; }

        public virtual Bloque Bloque { get; set; }
        public virtual ICollection<Respuesta> Respuesta { get; set; }
    }
}
