﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LezApi.Database;

namespace LezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreguntasExamenController : ControllerBase
    {
        private readonly LearnezContext _context;

        public PreguntasExamenController(LearnezContext context)
        {
            _context = context;
        }

        // GET: api/PreguntasExamen/1/1 - Obtiene una pregunta de examen con sus opciones- recibe examenId y numPregunta
        [HttpGet("{examenId}/{numPregunta}")]
        public async Task<ActionResult<PreguntaExamen>> GetPreguntaExamen(int examenId, short numPregunta)
        {
            var query = await _context.Preguntaexamen.FromSqlInterpolated($"SELECT * FROM PreguntaExamen Where NumPregunta = {numPregunta} AND ExamenId = {examenId}")
                .Include(op => op.Opcionexamen)
                .SingleOrDefaultAsync();

            if (query == null)
            {
                return NotFound();
            }
            else
            {
                return query;
            }
        }
    }
}
