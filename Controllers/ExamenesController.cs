﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LezApi.Database;

namespace LezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamenesController : ControllerBase
    {
        private readonly LearnezContext _context;

        public ExamenesController(LearnezContext context)
        {
            _context = context;
        }

        // GET: api/Examenes/Bloque/5 - Retorna el examen, el bloque y su libro correspondiente - recibe bloqueId
        [HttpGet("Bloque/{bloqueId}")]
        public async Task<ActionResult<Examen>> GetExamen(int bloqueId)
        {

            var query = await _context.Examen.FromSqlInterpolated($"SELECT * FROM Examen WHERE bloqueId = { bloqueId }")
                .Include(b => b.Bloque)
                .ThenInclude(l => l.Libro)
                .ThenInclude(g => g.Grado)
                .SingleOrDefaultAsync();

            if(query != null)
            {
                return query;
            }
            else
            {
                return NotFound();
            }

        }

    }
}
