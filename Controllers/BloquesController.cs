﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LezApi.Database;
using MySql.Data.MySqlClient;

namespace LezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BloquesController : ControllerBase
    {
        private readonly LearnezContext _context;

        public BloquesController(LearnezContext context)
        {
            _context = context;
        }

        // GET: api/Bloques - Obtiene todos los bloques, los libros relacionados y grado
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bloque>>> GetBloques()
        {
            return await _context.Bloque
                .Include(l => l.Libro)
                .ThenInclude(g => g.Grado)
                .ToListAsync();
        }

        // GET: api/Bloques/5 - Obtiene el bloque, el libro y el grado. En referencia al bloque solicitado, recibe bloqueId
        [HttpGet("{bloqueId}")]
        public async Task<ActionResult<Bloque>> GetBloque(int bloqueId)
        {
            var query = await _context.Bloque.FromSqlInterpolated($"SELECT * FROM Bloque WHERE bloqueId = {bloqueId}")
                 .Include(l => l.Libro)
                 .ThenInclude(g => g.Grado)
                 .SingleOrDefaultAsync();

            if (query != null)
            {
                return query;
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Bloques/Libro/5 - Obtiene los bloques de un libro y las páginas relacionadas, recibe libroId
        [HttpGet("libro/{libroId}")]
        public async Task<ActionResult<IEnumerable<Bloque>>> GetBloquesPorLibro(int libroId)
        {

            var query = await _context.Bloque.FromSqlInterpolated($"SELECT * FROM Bloque WHERE LibroId = {libroId}")
                .OrderBy(b => b.Nombre)
                .Include(p => p.Pagina)
                .Include(e => e.Examen)
                .ToListAsync();

            if (query != null)
            {
                return query;
            }
            else
            {
                return NotFound();
            }
        }


    }
}
