﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LezApi.Database;
using MySql.Data.MySqlClient;

namespace LezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaginasController : ControllerBase
    {
        private readonly LearnezContext _context;

        public PaginasController(LearnezContext context)
        {
            _context = context;
        }

        // GET: api/Paginas/numPagina/bloque/bloqueId - Obtiene la pagina y respuestas asociadas - recibe numero de página y bloqueId
        [HttpGet("{numPagina}/bloque/{bloqueId}")]
        public async Task<ActionResult<Pagina>> GetPagina(int numPagina, int bloqueId)
        {
            var query = await _context.Pagina.FromSqlInterpolated($"SELECT * FROM Pagina WHERE numPagina = {numPagina} AND bloqueId = {bloqueId}")
                .Include(r => r.Respuesta)
                .SingleOrDefaultAsync();

            if (query != null)
            {
                return query;
            } else
            {
                return NotFound();
            }

        }

        // POST: api/Paginas/add/[PaginaObject]
        [HttpPost("add")]
        public async Task<ActionResult<Pagina>> PostPagina(Pagina pagina)
        {
            _context.Pagina.Add(pagina);
            await _context.SaveChangesAsync();

            return Ok();
        }


    }
}
