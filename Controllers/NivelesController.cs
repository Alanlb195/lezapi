﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LezApi.Database;

namespace LezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NivelesController : ControllerBase
    {
        private readonly LearnezContext _context;

        public NivelesController(LearnezContext context)
        {
            _context = context;
        }

        // GET: api/Niveles - Obtiene todos los niveles educativos y sus grados asociados
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nivel>>> GetNiveles()
        {
            return await _context.Nivel
                .Include(g => g.Grado)
                .ToListAsync();
        }
    }
}
