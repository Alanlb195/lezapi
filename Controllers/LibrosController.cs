﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LezApi.Database;
using MySql.Data.MySqlClient;

namespace LezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibrosController : ControllerBase
    {
        private readonly LearnezContext _context;

        public LibrosController(LearnezContext context)
        {
            _context = context;
        }

        // GET: api/Libros/Grado/5 - Obtiene los libros y el grado del libro - recibe gradoId
        [HttpGet("grado/{gradoId}")]
        public async Task<ActionResult<IEnumerable<Libro>>> GetLibrosPorGrado(int gradoId)
        {
            var query = await _context.Libro.FromSqlInterpolated($"SELECT * FROM Libro WHERE libro.gradoId = {gradoId}")
                .OrderBy(t => t.Titulo)
                .Include(b => b.Grado)
                .ToListAsync();

            if (query != null)
            {
                return query;
            }
            else
            {
                return NotFound();
            }

        }

    }
}
